import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Tristan LE GACQUE
 * Created 14/11/2018
 */
public class Information {
    private static final String FILE = "/WEB-INF/info.properties";

    private final String jdbc;
    private final String user;
    private final String password;

    public Information(ServletContext context) {
        Properties prop = new Properties();

        try (InputStream is = context.getResourceAsStream(FILE)) {
            prop.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.jdbc = prop.getProperty("jdbc");
        this.user = prop.getProperty("user");
        this.password = prop.getProperty("pass");
    }

    public String getJdbc() {
        return jdbc;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
