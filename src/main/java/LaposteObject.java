/**
 * @author Tristan LE GACQUE
 * Created 21/11/2018
 */
public class LaposteObject {

    private String codeCommune;
    private String nomCommune;
    private String codePostal;
    private String libelle;
    private String ligne5;
    private String latitude;
    private String longitude;

    public LaposteObject() {

    }

    public String getCodeCommune() {
        return codeCommune;
    }

    public String getNomCommune() {
        return nomCommune;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getLigne5() {
        return ligne5;
    }

    public void setCodeCommune(String codeCommune) {
        this.codeCommune = codeCommune;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setNomCommune(String nomCommune) {
        this.nomCommune = nomCommune;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setLigne5(String ligne5) {
        this.ligne5 = ligne5;
    }

}
