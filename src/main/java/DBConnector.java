import javax.servlet.ServletContext;
import java.sql.*;

/**
 * @author Tristan LE GACQUE
 * Created 21/11/2018
 */
public class DBConnector {

    private static final String TABLE = "ville_france";


    public DBConnector() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection(ServletContext context){
        Information information = new Information(context);
        Connection con = null;
        try {
            con = DriverManager.getConnection(
                    information.getJdbc(),information.getUser(),information.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return con;
    }

    public LaposteObject getVille(Connection connection, String codeP) {
        String query = "SELECT * FROM " + TABLE + " WHERE Code_postal = ?";

        LaposteObject obj = null;

        try(PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, codeP);
            final ResultSet resultSet = ps.executeQuery();

            if(resultSet.next()) {
                obj = new LaposteObject();
                obj.setCodeCommune(resultSet.getString("Code_commune_INSEE"));
                obj.setNomCommune(resultSet.getString("Nom_commune"));
                obj.setCodePostal(resultSet.getString("Code_postal"));
                obj.setLibelle(resultSet.getString("Libelle_acheminement"));
                obj.setLigne5(resultSet.getString("Ligne_5"));
                obj.setLatitude(resultSet.getString("Latitude"));
                obj.setLongitude(resultSet.getString("Longitude"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return obj;
    }

}
