<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Calcul d'itinéraire</title>
</head>
<body>
<form method="GET" action="${pageContext.request.contextPath}/distance">
    <label for="cp1">Code postal de la ville 1 :</label>
    <input id=cp1" type="string" maxlength="5" placeholder="XXXXX" name="cp1">

    <label for="cp2">Code postal de la ville 2 :</label>
    <input id="cp2" type="string" maxlength="5" placeholder="XXXXX" name="cp2">

    <button type="submit">
        Calculer la distance
    </button>
</form>

<c:if test="${not empty MESSAGE }">
    <br /><br />
    <b>
        <c:out value="${MESSAGE}" />
    </b>
</c:if>

<c:if test="${not empty DISTANCE }">
    <br /><br />
    La distance entre <b>${C1}</b> et <b>${C2}</b> est de <b>${DISTANCE}</b> mètres !
</c:if>
</body>
</html>
